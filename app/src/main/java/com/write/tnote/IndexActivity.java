package com.write.tnote;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.write.tnote.data.Book;
import com.write.tnote.data.DataNotes;

import name.vbraun.view.write.Page;

public class IndexActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        DataNotes dataNotes = DataNotes.getInstance();
        PrepareLayout(dataNotes);


    }

    void PrepareLayout(final DataNotes dataNotes )
    {
        final Book book = dataNotes.book;
        LinearLayout rootLAyout =
                (LinearLayout) findViewById(R.id.root_index_layout);
        LayoutInflater inflto = getLayoutInflater();

        for(int  i = 0; i<= book.getPages().size()-1;i++) {
            final Page page = book.getPages().get(i);
            if(page.EnableIndex) {
                View view = inflto.inflate(R.layout.each_index_layout, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.index_image);
                imageView.setImageBitmap(page.getIndexBitmap());
                TextView textView = (TextView) view.findViewById(R.id.index_page_no);
                textView.setText(String.valueOf(book.getPages().indexOf(page)));
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dataNotes.comingFromIndex = true;
                        dataNotes.pageNo = book.getPages().indexOf(page);
                        Intent i = new Intent(IndexActivity.this, tnoteWriterActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                rootLAyout.addView(view);

            }

        }


    }
}
