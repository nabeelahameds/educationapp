package com.write.tnote;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.File;

/**
 * Created by PC on 11/20/2016.
 */

public class BaseConfig {
    public static String DATABASE_FILE_PATH = Environment.getExternalStorageDirectory().getPath()+"/eduappdb";
    public static String DATABASE_NAME=DATABASE_FILE_PATH + File.separator+ "educationapp.db";

    public static SQLiteDatabase GetDb(Context ctx)
    {
        SQLiteDatabase db = ctx.openOrCreateDatabase(DATABASE_NAME, 0, null, null);
        return db;
    }


    public static  String GetNotesNameFromId(String id,Context ctx)
    {


        String bedIdStr = "";
        try {





            SQLiteDatabase db = BaseConfig.GetDb(ctx);
            Cursor c = db.rawQuery("select * from notes where id  = '"+id.trim()+"'", null);


            if (c != null) {
                if (c.moveToFirst())
                {
                    do
                    {

                        bedIdStr = c.getString(c.getColumnIndex("name"));

                    } while (c.moveToNext());
                }
            }

            db.close();
            c.close();




        } catch (Exception e) {
            e.printStackTrace();
        }
        if(bedIdStr == null)
        {
            return "";
        }
        return  bedIdStr;
    }
}
