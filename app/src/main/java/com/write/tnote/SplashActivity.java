package com.write.tnote;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.write.tnote.education.LoginActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.write.tnote.BaseConfig.DATABASE_FILE_PATH;

public class SplashActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    Copydatabase copydb=new Copydatabase();

    ProgressBar progressBar;
    private int progress;
    private int progressStatus = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        copydb.execute();
    }








    private ProgressDialog pDialog;
    final Context context = this;

    private class Copydatabase extends AsyncTask<Void, Void, Void>
    {

        int firststatus;

        // //////////////////////////////////////////////
        protected void onPostExecute(Void v)
        {
            boolean initialiseDatabase = (new File(BaseConfig.DATABASE_NAME)).exists();
            if(initialiseDatabase) {
                finish();
                Intent intent = new Intent(getApplicationContext(), TaskMenu.class);
                startActivity(intent);
                Log.i("Splash to registration", "Splash to registration");
            }
        }








        @Override
        protected void onPreExecute()
        {


            super.onPreExecute();


            // Check if the database exists before copying
            boolean initialiseDatabase = (new File(BaseConfig.DATABASE_NAME)).exists();
            if (initialiseDatabase == false)
            {
            }
        }












        @Override
        protected Void doInBackground(Void... params)
        {



            try
            {

                shipdb();


            }
            catch (IOException e)
            {


                e.printStackTrace();

            }


            return null;

        }

        private void shipdb() throws IOException
        {

            File mParentDir = new File(BaseConfig.DATABASE_FILE_PATH);
            if(!mParentDir.exists())
            {
                mParentDir.mkdirs();

            }


            // Check if the database exists before copying
            boolean initialiseDatabase = (new File(BaseConfig.DATABASE_NAME)).exists();
            if (initialiseDatabase == false)
            {
                Log.i("Processing...","Copying Database");


                // Open the .db file in your assets directory
                InputStream is = getApplicationContext().getAssets().open("educationapp.db");

                // Copy the database into the destination
                OutputStream os = new FileOutputStream(BaseConfig.DATABASE_NAME);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0)
                {
                    os.write(buffer, 0, length);
                }
                os.flush();

                os.close();
                is.close();
                //First time initiating
                firststatus=0;


            }
            else
            {
                //First time initiating
                firststatus=1;
            }





        }
    }

}
