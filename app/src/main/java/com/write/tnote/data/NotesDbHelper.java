package com.write.tnote.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Nabeel on 10/30/2016.
 */

public class NotesDbHelper extends SQLiteOpenHelper {


    /**************************************************/
    // Constants
    /**************************************************/
    // The initial version of the application
    private static final int VERSION = 1;
    // Name of database
    private static final String DATABASE_NAME = "siaAppChallenge.db";
    // Tag for logging
    private static final String TAG = "SiaAppBaseHelper";

    /**************************************************/
    // Instance Variables
    /**************************************************/

    private static String DB_PATH = "";
    private SQLiteDatabase mDataBase;
    private  Context mContext;




   /* public NotesDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }*/

    public NotesDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, VERSION);
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;

    }





    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**************************************************/
    // Public Methods
    /**************************************************/
    public void createDB() throws IOException
    {
        //If the database does not exist, copy it from the assets.

        boolean mDataBaseExist = checkDB();
        if(!mDataBaseExist)
        {
            this.getReadableDatabase();
            this.close();
            try
            {
                //Copy the database from assets
                copyDB();
                Log.d(TAG, "Database successfully created.");
            }
            catch (IOException mIOException)
            {
                throw new Error("Error Copying DataBase");
            }
        }
    }

    public boolean openDB() throws SQLException
    {
        String mPath = DB_PATH + DATABASE_NAME;
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    @Override
    public synchronized void close()
    {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

    /**************************************************/
    // Private Methods
    /**************************************************/

    private boolean checkDB()
    {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDB() throws IOException
    {
        InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
        String outFileName = DB_PATH + DATABASE_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    void getAllNotes()
    {

    }



    void saveNotes(Book book)
    {
        String bookData = book.saveToDB();
        String UniqueId = book.GetBookUID();
        SQLiteDatabase dbOpen = this.getWritableDatabase();
        ContentValues values  = new ContentValues();
        values.put("note_strokes",bookData);
        values.put("uuid",UniqueId);
        values.put("","");




    }

    public  boolean CheckIsDataAlreadyInDBorNot(String query) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
       // String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }




}
