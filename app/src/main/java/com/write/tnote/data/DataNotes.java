package com.write.tnote.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Base64;

import com.write.tnote.BaseConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Random;
import java.util.UUID;

import name.vbraun.view.write.Page;

/**
 * Created by Nabeel on 10/30/2016.
 */
public class DataNotes {


    public String currentNoteId = "";
    public String currentNotesName = "";

    public boolean comingFromIndex =false;
    public int pageNo = -1;

    public static String DBPATH = Environment.getExternalStorageDirectory()+"/"+"tnote_10/educationapp";
    private static DataNotes ourInstance = new DataNotes();
    public Book book;


    public static DataNotes getInstance() {
        return ourInstance;
    }

    private DataNotes() {

    }

    private UUID currentBookUUId ;


    public UUID getCurrentUUID()
    {
        return this.currentBookUUId;
    }

    public void SetCurrentUUID(UUID uid)
    {
        this.currentBookUUId = uid;

    }

    public String convertBase64(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
       // to encode base64 from byte array use following method

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return  encoded;
    }

    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return 10000 + r.nextInt(20000);
    }

   public  void CreateNewNotewithRandomName(Context ctx)
    {

        String newNoteName = String.valueOf("New Note"+gen());
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",newNoteName);
        SQLiteDatabase db = BaseConfig.GetDb(ctx);
        currentNoteId =   String.valueOf(db.insert("notes",null,contentValues));


    }

    void saveNotes()
    {
        JSONObject rootObject = new JSONObject();
        for(int  i = 0 ; i <=book.pages.size()-1; i++)
        {
            Page page = book.pages.get(i);
            JSONObject pageObject = new JSONObject();
            try {

                pageObject.put("PAPER_TYPE",page);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }



}
