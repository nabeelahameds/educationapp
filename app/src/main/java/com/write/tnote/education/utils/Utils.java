package com.write.tnote.education.utils;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.write.tnote.R;


/**
 * Created by PC on 6/28/2016.
 */
public class Utils {

    public static Dialog getLoadingDialog(Context ctx, String title, String message)
    {
        Dialog mDialog  = new Dialog(ctx);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = LayoutInflater.from(ctx);
        View v = li.inflate(R.layout.wait_dialog,null,false);
        mDialog.setContentView(v);
        Rect displayRectangle = new Rect();
        Activity activity = (Activity) ctx;
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        v.setMinimumWidth((int)(displayRectangle.width() * 0.9f));

        final LinearLayout l1 = (LinearLayout) v.findViewById(R.id.linear1);
        final LinearLayout l2 = (LinearLayout) v.findViewById(R.id.linear2);
        final LinearLayout l3 = (LinearLayout) v.findViewById(R.id.linear3);
        final LinearLayout l4 = (LinearLayout) v.findViewById(R.id.linear4);

        int colorFrom = ctx.getResources().getColor(R.color.colorPrimaryDarkEpaper);
        int colorTo = ctx.getResources().getColor(R.color.colorPrimaryEpaper);
        final ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(1000); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
               // textView.setBackgroundColor((int) animator.getAnimatedValue());
                l1.setBackgroundColor((int) animator.getAnimatedValue());

            }

        });
        colorAnimation.start();

        final ValueAnimator colorAnimation1 = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation1.setDuration(2000); // milliseconds
        colorAnimation1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                // textView.setBackgroundColor((int) animator.getAnimatedValue());
                l2.setBackgroundColor((int) animator.getAnimatedValue());

            }

        });
        colorAnimation1.start();


        final ValueAnimator colorAnimation2 = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation2.setDuration(3000); // milliseconds
        colorAnimation2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                // textView.setBackgroundColor((int) animator.getAnimatedValue());
                l3.setBackgroundColor((int) animator.getAnimatedValue());

            }

        });
        colorAnimation2.start();
        final ValueAnimator colorAnimation3 = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation3.setDuration(4000); // milliseconds
        colorAnimation3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                // textView.setBackgroundColor((int) animator.getAnimatedValue());
                l4.setBackgroundColor((int) animator.getAnimatedValue());

            }

        });
        colorAnimation3.start();


        colorAnimation3.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                colorAnimation.start();
                colorAnimation1.start();
                colorAnimation2.start();
                colorAnimation3.start();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        return  mDialog;
    }
}
