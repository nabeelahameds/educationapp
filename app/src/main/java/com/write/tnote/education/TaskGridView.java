package com.write.tnote.education;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.write.tnote.R;
import com.write.tnote.TimeTable;
import com.write.tnote.education.eduactivities.Inbox;
import com.write.tnote.education.eduactivities.NewNotes;
import com.write.tnote.education.eduactivities.NoticeActivity;


public class TaskGridView extends AppCompatActivity {


    public String[] prgmNameList = {
            "Time Table",
            "Notes",
            "Class Notes",
            "Notice Board",
            "Inbox",
            "Groups",
            "Books",

    };

    public int[] prgmImages = {
            R.drawable.schedule,
            R.drawable.notes,
            R.drawable.class_notes,
            R.drawable.noticeboard,
            R.drawable.inbox,
            R.drawable.peers,
            R.drawable.book,


            };
    private GridView grid;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        GetInitialize();
        LoadGridView();
        initUI();




    }



    private void initUI() {

        grid = (GridView) findViewById(R.id.task_grid);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                switch (prgmNameList[position]) {

                    case "Time Table":
                        finish();
                        Intent i = new Intent(TaskGridView.this, TimeTable.class);
                        startActivity(i);
                        break;

                    case "Notes":
                       // finish();
                        Intent i1 = new Intent(TaskGridView.this, NewNotes.class);
                        startActivity(i1);
                        break;


                    case "Notice Board":
                        Intent i2 = new Intent(TaskGridView.this, NoticeActivity.class);
                        startActivity(i2);


                        break;
                    case "Inbox":
                        Intent i3 = new Intent(TaskGridView.this, Inbox.class);
                        startActivity(i3);



                        break;
                    case "Settings":



                        break;
                    case "Help":
                        break;
                    case "LogOut":

                        break;

                    case "Donors":
                        break;
                    case "Places":
                        break;
                    case " ":
                        break;


                }

            }
        });
    }

    void showPopup()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("LogOut");
        builder.setMessage("Are you sure want to exit?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();

            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        builder.show();
    }


    private void GetInitialize() {

        grid = (GridView) findViewById(R.id.task_grid);
        //setReminder();
    }


    private void LoadGridView() {

        CustomHelpListAdapter1 adapter = new CustomHelpListAdapter1(this, prgmNameList, prgmImages);
        grid.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(TaskGridView.this);
        dialog.setTitle("Education App");
        dialog.setMessage("Do you want to exit?");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();

            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();




    }

    @Override
    protected void onResume() {
        super.onResume();

    }




}
