package com.write.tnote.education;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.write.tnote.R;
import com.write.tnote.education.utils.Utils;


public class LoginActivity extends AppCompatActivity {

    Button loginBtn;
    Button cancelBtn;
    EditText idEdit;
    EditText pwdEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initUI();
        initList();






    }



    private void initUI() {

        loginBtn = (Button) findViewById(R.id.login);
        idEdit= (EditText) findViewById(R.id.email_id);
        pwdEdit= (EditText) findViewById(R.id.pwd_edit);
        cancelBtn = (Button) findViewById(R.id.cancel_btn);
    }
    private void initList() {
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
                dialog.setTitle("Education App");
                dialog.setMessage("Do you want to exit?");
                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();

                    }
                });
                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.show();

            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idEdit.getText().length() > 0 && pwdEdit.getText().length() > 0)
                {
                    if(idEdit.getText().toString().equals("student") && pwdEdit.getText().toString().equalsIgnoreCase("student"))
                    {
                        new AuthLogin("","").execute("");
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this,"Email Id & Password combination wrong", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this,"input both fields", Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    public class AuthLogin extends AsyncTask<String,String, String>
    {
        Dialog mdialog;
        public AuthLogin(String mUsername, String mPassword)
        {


        }

        @Override
        protected void onPreExecute() {
            mdialog = Utils.getLoadingDialog(LoginActivity.this,"","");
            mdialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            mdialog.dismiss();
            super.onPostExecute(s);
            Intent i = new Intent(LoginActivity.this, TaskGridView.class);
            startActivity(i);
            finish();


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
        dialog.setTitle("Education App");
        dialog.setMessage("Do you want to exit?");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();

            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();


    }
}

