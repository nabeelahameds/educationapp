package com.write.tnote.education.eduactivities;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.write.tnote.BaseConfig;
import com.write.tnote.R;
import com.write.tnote.data.DataNotes;
import com.write.tnote.tnoteWriterActivity;

;

public class NewNotes extends AppCompatActivity {

    LinearLayout rootLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notes);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                DataNotes dataNotes = DataNotes.getInstance();

                dataNotes.currentNoteId = "";



                Intent i = new Intent(NewNotes.this, tnoteWriterActivity.class);
                startActivity(i);

            }
        });

       rootLinearLayout = (LinearLayout) findViewById(R.id.root_layout);
        loadNotes();





    }



    public   void loadNotes()
    {

        LayoutInflater inflto = getLayoutInflater();



        String name = "";
        try {





            SQLiteDatabase db = BaseConfig.GetDb(this);
            Cursor c = db.rawQuery("select * from notes", null);


            if (c != null) {
                if (c.moveToFirst())
                {
                    do
                    {

                       String nameNotes =  c.getString(c.getColumnIndex("name"));
                        View view = inflto.inflate(R.layout.each_notes_layout, null);

                        if(nameNotes.length() > 1)
                        {

                            TextView notesTextView = (TextView) view.findViewById(R.id.idnnote);

                            notesTextView.setText(name);

                            view.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {

                                    showNotesOptions();

                                    return false;
                                }


                            });
                            rootLinearLayout.addView(view);


                        }



                    } while (c.moveToNext());
                }
            }


            db.close();
            c.close();




        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showNotesOptions() {


        final CharSequence[] items = {
                "Rename", "delete","Close"
        };

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].toString().equalsIgnoreCase("Rename")) {  }

                 else if (items[item].toString().equalsIgnoreCase("Delete"))
                {




                }

                else if (items[item].toString().equalsIgnoreCase("Close")) {



                }




            }
        });
        android.support.v7.app.AlertDialog alert = builder.create();
        Dialog dialog = alert;

        dialog.show();
    }


}
