package com.write.tnote.education.eduactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.write.tnote.R;


public class Inbox extends AppCompatActivity {


    LinearLayout rootLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_inbox);

        rootLinearLayout = (LinearLayout) findViewById(R.id.root_layout);

        LayoutInflater inflto = getLayoutInflater();

        for(int  i = 0; i<= 10;i++) {
            View view = inflto.inflate(R.layout.each_inbox, null);
            rootLinearLayout.addView(view);
        }

    }

}
