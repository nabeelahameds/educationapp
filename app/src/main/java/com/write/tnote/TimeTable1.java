package com.write.tnote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class TimeTable1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table1);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent i = new Intent(this, TaskMenu.class);
        startActivity(i);
    }

}
